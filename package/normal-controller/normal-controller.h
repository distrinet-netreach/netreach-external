#include <linux/types.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/module.h>

#ifndef NORMAL_CONTROLLER
#define NORMAL_CONTROLLER

int norm_ssp_register(uuid_t uuid, void (*notif_handler)(void));
int norm_ssp_register_dev(uuid_t uuid, void *dev_id,
			  void (*notif_handler)(void *));
int norm_ssp_register_dev_fast(void *dev_id, void (*notif_handler)(void *));

#endif /* NORMAL_CONTROLLER */