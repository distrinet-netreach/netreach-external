#include <linux/stddef.h>
#include <linux/ktime.h>
#include <linux/printk.h>
#include <linux/kernel.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/module.h>
#include <linux/tee_drv.h>

#include <normal-controller.h>
#include <secure_ethernet_driver_public.h>

#define DRIVER_NAME "Normal world Secure Ethernet driver"
#define DEVICE_NAME "secure-ethernet-driver"

#define MAX_PACKET_SIZE 1518

static const uuid_t sec_eth_uuid =
	UUID_INIT(UUID1, UUID2, UUID3, UUID4, UUID5, UUID6, UUID7, UUID8, UUID9,
		  UUID10, UUID11);

static struct sec_eth_driver {
	u32 sess_id;
	struct tee_context *ctx;
} driver;

static unsigned char mac[6] = { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55 };

struct sec_eth_packet {
	u16 len;
	u8 data[];
};

struct sec_eth_buffer {
	u8 buf[1024 * 16 * sizeof(u8)];
	unsigned int size;
	unsigned int read;
	unsigned int write;
	unsigned int watermark;
};

struct sec_eth_priv {
	int status;
	struct sec_eth_buffer *rx_buffer;
	struct sec_eth_buffer *tx_buffer;
};

struct net_device *sec_eth_dev;

static int sec_eth_open(struct net_device *);
static int sec_eth_release(struct net_device *);
static netdev_tx_t sec_eth_tx(struct sk_buff *, struct net_device *);
static void sec_eth_rx(struct net_device *, struct sec_eth_packet *pkt);
static void print_packet(const uint8_t *const packets, const uint16_t len);

static const struct net_device_ops sec_eth_ops = {
	.ndo_open = sec_eth_open,
	.ndo_stop = sec_eth_release,
	.ndo_start_xmit = sec_eth_tx,
};

static struct tee_shm *shared_rx, *shared_tx;

static int sec_eth_open(struct net_device *dev)
{
	struct sec_eth_priv *priv = netdev_priv(dev);
	// struct tee_shm *shared_rx, *shared_tx;
	struct sec_eth_buffer *buffer;
	struct tee_ioctl_invoke_arg inv_arg;
	struct tee_param param[4];
	int ret = 0;

	pr_info("Opening ethernet\n");

	// Allocate rx buffer
	shared_rx = tee_shm_alloc(driver.ctx, sizeof(struct sec_eth_buffer),
				  (TEE_SHM_MAPPED | TEE_SHM_DMA_BUF));
	if (IS_ERR(shared_rx)) {
		pr_err("Failed to map shared memory: %lx\n",
		       PTR_ERR(shared_rx));
		return PTR_ERR(shared_rx);
	}
	priv->rx_buffer = tee_shm_get_va(shared_rx, 0);
	if (IS_ERR(buffer)) {
		pr_err("tee_shm_get_va failed: %lx\n", PTR_ERR(buffer));
		return PTR_ERR(buffer);
	}

	memset(priv->rx_buffer, 0, sizeof(struct sec_eth_buffer));
	priv->rx_buffer->size = sizeof_field(struct sec_eth_buffer, buf);
	priv->rx_buffer->watermark = priv->rx_buffer->size;

	// Allocate tx buffer
	shared_tx = tee_shm_alloc(driver.ctx, sizeof(struct sec_eth_buffer),
				  (TEE_SHM_MAPPED | TEE_SHM_DMA_BUF));
	if (IS_ERR(shared_tx)) {
		pr_err("Failed to map shared memory: %lx\n",
		       PTR_ERR(shared_tx));
		return PTR_ERR(shared_tx);
	}
	priv->tx_buffer = tee_shm_get_va(shared_tx, 0);
	if (IS_ERR(buffer)) {
		pr_err("tee_shm_get_va failed: %lx\n", PTR_ERR(buffer));
		return PTR_ERR(buffer);
	}

	memset(priv->tx_buffer, 0, sizeof(struct sec_eth_buffer));
	priv->tx_buffer->size = sizeof_field(struct sec_eth_buffer, buf);
	priv->tx_buffer->watermark = priv->tx_buffer->size;

	// Initializing SW
	memset(&inv_arg, 0, sizeof(inv_arg));
	memset(&param, 0, sizeof(param));

	inv_arg.func = SEC_ETH_CMD_INIT;
	inv_arg.session = driver.sess_id;
	inv_arg.num_params = 4;
	param[0].attr = TEE_IOCTL_PARAM_ATTR_TYPE_MEMREF_INOUT;
	param[0].u.memref.shm = shared_rx;
	param[0].u.memref.size = sizeof(struct sec_eth_buffer);
	param[0].u.memref.shm_offs = 0;
	param[1].attr = TEE_IOCTL_PARAM_ATTR_TYPE_MEMREF_INOUT;
	param[1].u.memref.shm = shared_tx;
	param[1].u.memref.size = sizeof(struct sec_eth_buffer);
	param[1].u.memref.shm_offs = 0;

	ret = tee_client_invoke_func(driver.ctx, &inv_arg, param);
	if ((ret < 0) || (inv_arg.ret != 0)) {
		pr_err("SEC_ETH_CMD_INIT invoke error: %x, %x.\n", inv_arg.ret,
		       inv_arg.ret_origin);
		return -EINVAL;
	}

	memcpy(dev->dev_addr, mac, ETH_ALEN);
	netif_start_queue(dev);

	return 0;
}

static int sec_eth_release(struct net_device *dev)
{
	netif_stop_queue(dev); /* can't transmit any more */
	return 0;
}

static unsigned int buffer_free(struct sec_eth_buffer *buf)
{
	if (buf->write >= buf->read) {
		return buf->size - buf->write;
	} else {
		return buf->read - buf->write;
	}
}

static int copy_to_buffer(struct sec_eth_buffer *buf, void *data,
			  unsigned int len, unsigned int pkt_len)
{
	unsigned int free = buffer_free(buf);
	unsigned int len_in_buf =
		pkt_len + sizeof_field(struct sec_eth_packet, len);
	struct sec_eth_packet *pkt;

	// Fail if buffer is full.
	// Deal with wraparound now
	if (buf->write >= buf->read && free < len_in_buf &&
	    buf->read <= len_in_buf) { // Don't overtake read index
		return -1;
	}

	// Don't overtake read index by not allowing free to equal length.
	if (buf->write < buf->read && free <= len_in_buf) {
		return -1;
	}

	// Shift to start of buffer if too small.
	if (free < len_in_buf) {
		buf->watermark = buf->write;
		buf->write = 0;
	}

	pkt = (struct sec_eth_packet *)&(buf->buf[buf->write]);

	memcpy(pkt->data, data, len);

	// packet should be padded with 0
	if (len < pkt_len) {
		memset(pkt->data + len, 0, pkt_len - len);
	}

	pkt->len = pkt_len;
	buf->write += len_in_buf;

	return pkt_len;
}

static netdev_tx_t sec_eth_tx(struct sk_buff *skb, struct net_device *dev)
{
	struct sec_eth_priv *priv = netdev_priv(dev);
	struct tee_ioctl_invoke_arg inv_arg;
	struct tee_param param[4];
	unsigned int pkt_len;
	int ret;
	ktime_t start;

	// pr_info("length: %u\n", skb->data_len);
	// print_packet(skb->data, skb->data_len);

	// Copy data and adjust write pointer.
	if (skb->len < ETH_ZLEN) {
		pkt_len = ETH_ZLEN;
	} else {
		pkt_len = skb->len;
	}
	ret = copy_to_buffer(priv->tx_buffer, skb->data, skb->len, pkt_len);
	if (ret < 0) {
		return NETDEV_TX_BUSY;
	}
	dev_kfree_skb(skb);

	memset(&inv_arg, 0, sizeof(inv_arg));
	memset(&param, 0, sizeof(param));

	inv_arg.func = SEC_ETH_CMD_SEND;
	inv_arg.session = driver.sess_id;
	inv_arg.num_params = 0;

	do {
		ret = tee_client_invoke_func(driver.ctx, &inv_arg, param);
		if ((ret < 0) || (inv_arg.ret != 0)) {
			if (inv_arg.ret == 0xffff000d) { // TEE_ERROR_BUSY
				pr_info("ETH_SEND invoke returned TEE_ERROR_BUSY\n");
				if (!netif_queue_stopped(dev)) {
					netif_stop_queue(dev);
				}
				mdelay(1);
				continue;
			}
			pr_err("ETH_SEND invoke error: %x from %x.\n",
			       inv_arg.ret, inv_arg.ret_origin);
			return -EINVAL;
		}
	} while (inv_arg.ret);

	if (netif_queue_stopped(dev)) {
		netif_wake_queue(dev);
	}

	return NETDEV_TX_OK;
}

#define TEEC_ERROR_EXCESS_DATA 0xFFFF0004

static void notif_handler(void *dev_id)
{
	// Notification received from secure world ==> Frame available
	struct sec_eth_priv *priv = netdev_priv((struct net_device *)dev_id);
	struct sec_eth_buffer *buf = priv->rx_buffer;
	struct sec_eth_packet *pkt;
	int ret;
	u64 start;

	while (buf->read != buf->write) {
		if (buf->read == buf->watermark) {
			buf->read = 0;
			buf->watermark = buf->size;
		}

		pkt = (struct sec_eth_packet *)&buf->buf[buf->read];

		// send it to sec_eth_rx for handling
		// start = ktime_get();
		sec_eth_rx(sec_eth_dev, pkt);
		// pr_info("Send packet to Linux: %lld µs\n",
		// 	ktime_us_delta(ktime_get(), start));

		buf->read += sizeof(struct sec_eth_packet) + pkt->len;
	}
}

static void print_packet(const uint8_t *const packets, const uint16_t len)
{
	u16 i;
	int j;

	for (i = 0; i < len; i += 16) {
		char buf[512];
		size_t boffs = 0;
		int res;
		res = snprintf(buf, sizeof(buf), "%04x: ", i);

		for (j = 0; j < 16; j++) {
			boffs += res;
			if (i + j >= len) {
				goto out;
			}
			res = snprintf(buf + boffs, sizeof(buf) - boffs,
				       "%02x ", packets[i + j]);
			if (j == 7) {
				buf[boffs + res] = ' ';
				res++;
			}
		}

		boffs += res;
		buf[boffs] = ' ';
		buf[boffs + 1] = ' ';
		buf[boffs + 2] = ' ';

		for (j = 0; j < 16; j++) {
			boffs += res;
			if (i + j >= len) {
				goto out;
			}
			if (packets[i + j] > 31 && packets[i + j] < 127) {
				res = snprintf(buf + boffs, sizeof(buf) - boffs,
					       "%c", packets[i + j]);
			} else {
				buf[boffs] = '.';
				res = 1;
			}

			if (j == 7) {
				buf[boffs + res] = ' ';
				res++;
			}
		}
		boffs += res;

	out:
		if (boffs >= (sizeof(buf) - 1))
			boffs = sizeof(buf) - 2;

		buf[boffs] = '\n';
		while (boffs && buf[boffs] == '\n')
			boffs--;
		boffs++;
		buf[boffs + 1] = '\0';

		pr_info("%s", (char *)buf);
	}
}

static void sec_eth_rx(struct net_device *dev, struct sec_eth_packet *pkt)
{
	struct sk_buff *skb;
	struct sec_eth_priv *priv = netdev_priv(dev);

	// print_packet(pkt->data, pkt->len);

	/*
	 * The packet has been retrieved from the transmission
	 * medium. Build an skb around it, so upper layers can handle it
	 */
	skb = dev_alloc_skb(pkt->len);
	if (!skb) {
		if (printk_ratelimit())
			printk(KERN_NOTICE
			       "eth ssp rx: low on mem - packet dropped\n");
		goto out;
	}
	memcpy(skb_put(skb, pkt->len), pkt->data, pkt->len);

	/* Write metadata, and then pass to the receive level */
	skb->dev = dev;
	skb->protocol = eth_type_trans(skb, dev);
	netif_rx(skb);
out:
	return;
};

static int register_device(void)
{
	int ret;
	struct sec_eth_priv *priv;

	sec_eth_dev = alloc_etherdev(sizeof(struct sec_eth_priv));

	if (sec_eth_dev == NULL) {
		return -1;
	}

	sec_eth_dev->netdev_ops = &sec_eth_ops;

	if ((ret = register_netdev(sec_eth_dev))) {
		pr_err("Error %i registering eth ssp device \n", ret);
		return ret;
	}

	priv = netdev_priv(sec_eth_dev);
	memset(priv, 0, sizeof(struct sec_eth_priv));

	if ((ret = norm_ssp_register_dev_fast(sec_eth_dev, notif_handler)) <
	    0) {
		pr_err("Could not register with the normal controller\n.");
		return ret;
	}

	return 0;
}

static int unregister_device(void)
{
	if (sec_eth_dev) {
		unregister_netdev(sec_eth_dev);
		free_netdev(sec_eth_dev);
	}
	return 0;
}

static int open_session(const char *name, u32 *sess_id)
{
	struct tee_ioctl_open_session_arg sess_arg;
	struct tee_param param[4];
	int ret;

	memset(&sess_arg, 0, sizeof(sess_arg));
	export_uuid(sess_arg.uuid, &sec_eth_uuid);
	sess_arg.clnt_login = TEE_IOCTL_LOGIN_PUBLIC;
	sess_arg.num_params = 4;

	memset(&param, 0, sizeof(param));

	ret = tee_client_open_session(driver.ctx, &sess_arg, param);
	if ((ret < 0) || (sess_arg.ret != 0)) {
		pr_err("tee_client_open_session failed for device %s, err: %x\n",
		       name, sess_arg.ret);
		return -EINVAL;
	}
	*sess_id = sess_arg.session;

	return 0;
}

static void close_session(void)
{
	tee_client_close_session(driver.ctx, driver.sess_id);
}

static int optee_ctx_match(struct tee_ioctl_version_data *ver, const void *data)
{
	if (ver->impl_id == TEE_IMPL_ID_OPTEE)
		return 1;
	else
		return 0;
}

static int create_context(void)
{
	driver.ctx = tee_client_open_context(NULL, optee_ctx_match, NULL, NULL);
	if (IS_ERR(driver.ctx))
		return -ENODEV;

	return 0;
}

static void destroy_context(void)
{
	tee_client_close_context(driver.ctx);
}

static int __init mod_init(void)
{
	int ret;

	if ((ret = create_context()) < 0) {
		pr_err("Could not create a context.\n");
		return ret;
	}
	if ((ret = open_session(DRIVER_NAME, &driver.sess_id)) < 0) {
		pr_err("Could not open a session\n.");
		return ret;
	}
	if ((ret = register_device()) < 0) {
		pr_err("Could not register the device\n.");
		return ret;
	}

	return 0;
}

static void __exit mod_exit(void)
{
	unregister_device();

	destroy_context();
	close_session();
}

module_init(mod_init);
module_exit(mod_exit);

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Arne Deprez");
