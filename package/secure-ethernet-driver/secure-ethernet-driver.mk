################################################################################
#
# secure-ethernet-driver
#
################################################################################

SECURE_ETHERNET_DRIVER_VERSION = 0.1
SECURE_ETHERNET_DRIVER_SITE = $(BR2_EXTERNAL_SECURE_ETHERNET_DRIVER_PATH)/package/secure-ethernet-driver
SECURE_ETHERNET_DRIVER_SITE_METHOD = local

SECURE_ETHERNET_DRIVER_DEPENDECIES = optee-os normal-controller

SECURE_ETHERNET_DRIVER_MODULE_MAKE_OPTS = \
	OPTEE_OS_SRC_DIR=$(OPTEE_OS_DIR) \
	NORMAL_CONTROLLER_SRC_DIR=$(NORMAL_CONTROLLER_DIR)

define SECURE_ETHERNET_DRIVER_INSTALL_SCRIPT
        $(INSTALL) -m 0755 -D $(SECURE_ETHERNET_DRIVER_PKGDIR)setup-network.sh \
                $(TARGET_DIR)/usr/sbin/setup-network.sh
endef

SECURE_ETHERNET_DRIVER_POST_INSTALL_TARGET_HOOKS += SECURE_ETHERNET_DRIVER_INSTALL_SCRIPT

$(eval $(kernel-module))
$(eval $(generic-package))