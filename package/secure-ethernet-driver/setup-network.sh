#! /bin/sh

modprobe normal-controller
modprobe secure-ethernet-driver
ip addr add 192.168.0.100/24 dev eth1
ip link set up dev eth1
